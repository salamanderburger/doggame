﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DogGame
{
    public class Bulldog : Dog
    {
        
        
        

        public Bulldog(string n, int a)
        {
            name = n;
            age = a;
            canbark = true;
            canplaydead = true;
            gooddog = true;
            breed = "Bulldog";
        }
        public string BulldogTrick(int command)
        {
            return name + " " + Trick(command);
            
        }
    }
}