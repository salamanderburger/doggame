﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DogGame
{
    public class Chihuahua : Dog
    {
        //This is a subclass of dog, called Chihuahua.
        //You can think of it like this
        //"all chihuahuas are dogs, not all dogs are chihuahuas".


        //If there are other pieces of information that would make more sense
        //to describe as relating to a chihuahua
        //but not a general dog, we would put that information here as a field.
        //Eg (breed specific health complications, if they are a guard dog,
        //the size of the bag you carry the dog in, etc.)
        

        //a constructor function is applied to this subclass,
        //dynamic information: name, age (set by the webform input)
        //static information: canbark, gooddog, breed (automatically set)

        public Chihuahua(string n, int a)
        {
            name = n;
            age = a;
            canbark = true;
            gooddog = true;
            canplaydead = true;
            breed = "Chihuahua";
        }
        // We would call this a wrapper function
        // It holds the function dog.Trick() as chihuahau.ChihuahuaTrick();
        // the reason for this is because we want to include the name of the dog
        // the name of the dog is included in the sub class, but not the base class.
        public string ChihuahuaTrick(int command)
        {
            return name + " " + Trick(command);
        }
    }
}