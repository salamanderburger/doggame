﻿//Adding the Rotweiler class.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DogGame
{
    public class Rotweiler : Dog
    {
        public Rotweiler(string n, int a)
        {
            name = n;
            age = a;
            gooddog = true;
            canplaydead = true;
            canbark = true;
            canrollover = false;
            breed = "Rotweiler";
        }

        public string RotweilerTrick(int command)
        {
            return name + " " + Trick(command);
        }
    }
}