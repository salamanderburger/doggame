﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DogGame
{
    public class Dog
    {
        //This is the base class of dog. You would expect fields here to be
        //generic among all dogs.

        public string name;
        public int age;
        public string breed;

        public bool gooddog;
        public bool canbark;
        public bool canrollover;
        public bool canshake;
        public bool canplaydead;
        public bool canfetch;

        //Constructor function for dog.
        public Dog()
        {
            gooddog = true;
            canbark = true;
            canrollover = false;
            canshake = false;
            canplaydead = false;
        }
        //method for dog called "trick"
        //If a dog is a good dog, they will try to do the trick
        //else they will ignore you.
        //If they can do the trick, they will,
        //else there is a message saying they can't do the trick.
        public string Trick (int command)
        {
            string res = "";
            if (!gooddog)
            {
                res = "Ignored you.";
                return res;
            }            
            switch (command)
            {
                case 1:
                    if (canbark) res = "Barked!";
                    else res = "doesn't know how to bark";
                    break;
                case 2:
                    if (canrollover) res = "Rolled Over!";
                    else res = "doesn't know how to roll over";
                    break;
                case 3:
                    if (canshake) res = "Shook!";
                    else res = "doesn't know how to shake";
                    break;
                case 4:
                    if (canplaydead) res = "Played Dead!";
                    else res = "doesn't know how to play dead";
                    break;
                case 5:
                    if (canfetch) res = "Fetched The Toy!";
                    else res = "doesn't know how to fetch.";
                    break;
            }
            return res;
        }

        public string Train(int trick)
        {
            string res = "";
            if (age>6)
            {
                res = "can't teach an old dog new tricks";
                return res;
            }
            switch(trick)
            {
                case 1:
                    canbark = true;
                    res = "learned to bark!";
                    break;
                case 2:
                    canrollover = true;
                    res = "learned to roll over!";
                    break;
                case 3:
                    canshake = true;
                    res = "learned to shake!";
                    break;
                case 4:
                    canplaydead = true;
                    res = "learned to play dead!";
                    break;
                case 5:
                    canfetch = true;
                    res = "learned to fetch!";
                    break;
            }

            return res;
        }

        public string Info()
        {
            //you can fill out moreinformation if you want.
            string info = "Your dog is " + age + " years old";
            info += "<br>Their name is " + name;
            if(gooddog)
            {
                info += "<br>They are a good dog";

            }
            return info;
        }
        
    }
}