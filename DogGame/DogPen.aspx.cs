﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DogGame
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if we wanted to run some server side processing
            //before the page is loaded onto the browser, we would put that code here
        }

        protected void command_dog(object sender, EventArgs e)
        {
            //gather all of the variables from the web form

            //we imply the following about dogbreeds and commands
            //dogbreed: 1=>chihuahua, 2=>bulldog, 3=>german shepherd, 4=>rotweiler
            //command: 1=>bark, 2=>rollover, 3=>shake, 4=>play dead

            string myDogName = dogName.Text.ToString();
            int myDogAge = Int32.Parse(dogAge.Text);
            int myDogBreed = Int32.Parse(dogBreed.SelectedValue);
            int command = Int32.Parse(dogCommand.SelectedValue);

            /*
             * This is debugging code, just to make sure that we in fact do grab the
             * right information.
            dogRes.InnerHtml = "Creating a dog with breed of " + myDogBreed.ToString()
                + ", age: " + myDogAge.ToString() +", and name: "+myDogName+"<br>";
                */

            //This is a switch statement. It like a series
            //of if statements put together.
            switch (myDogBreed)
            {
                case 1:
                    Chihuahua mychihuahua = new Chihuahua(myDogName, myDogAge);
                    //mychihuahua.Train(4);
                    dogRes.InnerHtml = mychihuahua.ChihuahuaTrick(command);


                    break;
                case 2:
                    Bulldog mybulldog = new Bulldog(myDogName, myDogAge);
                    //Teaches the bull dog to rollover and fetch
                    dogRes.InnerHtml = mybulldog.Train(2);
                    dogRes.InnerHtml += mybulldog.Train(5);
                    dogRes.InnerHtml += mybulldog.BulldogTrick(command);
                    break;
                case 3:
                    Shepherd myshepherd = new Shepherd(myDogName, myDogAge);
                    dogRes.InnerHtml = myshepherd.ShepherdTrick(command);
                    break;
                case 4:
                    Rotweiler myrotweiler = new Rotweiler(myDogName, myDogAge);
                    dogRes.InnerHtml = myrotweiler.RotweilerTrick(command);
                    break;

            }
        }
    }
}