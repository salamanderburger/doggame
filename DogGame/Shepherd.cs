﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DogGame
{
    public class Shepherd : Dog
    {
        
       

        public Shepherd(string n, int a)
        {
            name = n;
            age = a;
            canbark = true;
            gooddog = true;
            breed = "Shepherd";
        }
        public string ShepherdTrick(int command)
        {
            return name + " " + Trick(command);
        }
    }
}