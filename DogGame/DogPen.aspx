﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DogPen.aspx.cs" Inherits="DogGame.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Dog Game</h1>
            
            <div>
                <label for="dogName">Dog Name: </label><asp:TextBox ID="dogName" runat="server"></asp:TextBox>
            </div>
            <div>
                <label for="dogAge">Dog Age: </label><asp:TextBox ID="dogAge" runat="server"></asp:TextBox>
            </div>
            <div>
                <label for="dogBreed">Dog Breed: </label>
                <asp:DropDownList 
                    ID="dogBreed" 
                    runat="server">
                    <asp:ListItem Text="Chihuahua" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Bulldog" Value="2"></asp:ListItem>
                    <asp:ListItem Text="German Shephard" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Rotweiler" Value="4"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <label for="dogCommand">Trick: </label>
                <asp:DropDownList
                    ID="dogCommand"
                    runat="server">
                    <asp:ListItem Text="Bark" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Roll Over" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Shake" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Play Dead" Value="4"></asp:ListItem>
                    <asp:ListItem Text="Fetch" Value="5"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <asp:Button runat="server" OnClick="command_dog" Text="Do the trick!"/>
            <div id="dogRes" runat="server">

            </div>
        </div>
    </form>
</body>
</html>
